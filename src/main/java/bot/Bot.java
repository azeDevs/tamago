package main.java.bot;

import main.java.bot.managers.DiscordManager;
import main.java.bot.managers.InputManager;
import main.java.bot.managers.OutputManager;

public class Bot {

    public static final boolean DEBUGGING = true;

    // Bot Interface
    public static final String TOKEN = "MzkyMDAxNTM1MDI0MDM3OTA5.DRg47w.T5uBavbVArIk447L671t1L5V9GI";
    public static final String ALIAS = "<@392001535024037909>";

    // User Interface
    public static final String PLAYING = "\uD83D\uDD34 SENTRY MODE";
    public static final String TRIGGER = "tama";

    // Stats
    private static final int MAX_HITPOINTS = 8;
    private static final int MAX_NUTRITION = 8;
    private static int hitpoints = 5;
    private static int nutrition = 5;

    public static void init() {
        InputManager.getInstance();
        OutputManager.getInstance();
        DiscordManager.getInstance().init();
    }

    public static void changeHitpoints(int amount) {
        hitpoints += amount;
        if (hitpoints > MAX_HITPOINTS) hitpoints = MAX_HITPOINTS;
        if (hitpoints < 0) hitpoints = 0;
    }

    public static void changeNutrition(int amount) {
        nutrition += amount;
        if (nutrition > MAX_NUTRITION) nutrition = MAX_NUTRITION;
        if (nutrition < 0) nutrition = 0;
    }

}
