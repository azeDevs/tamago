package main.java.bot.managers;

import de.btobastian.javacord.entities.message.Message;
import static main.java.bot.Bot.TRIGGER;

public class InputManager {

    private static final InputManager instance = new InputManager();

    private InputManager() { }

    public static InputManager getInstance() {
        return instance;
    }

    public void scanForTriggers(Message message) {

        /* TODO: have all outputs end on the same message.reply method call
           so that they can be prevented from sending when there is no valid bot response.
        */

        if (hasKeyTrigger(message)) message.reply(OutputManager.getInstance().getKeyTriggeredResponse(message));
        message.reply(OutputManager.getInstance().getFreeTriggeredResponse(message));

    }

    private boolean hasKeyTrigger(Message message) {
        return message.getContent().length() >= 3 && message.getContent().substring(0, 4).equals(TRIGGER);
    }


}
