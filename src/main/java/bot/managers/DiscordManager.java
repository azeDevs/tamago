package main.java.bot.managers;

import com.google.common.util.concurrent.FutureCallback;
import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.listener.message.MessageCreateListener;

import static main.java.bot.Bot.PLAYING;
import static main.java.bot.Bot.TOKEN;

public class DiscordManager {

    private static final DiscordAPI discordApi = Javacord.getApi(TOKEN, true);
    private static final DiscordManager instance = new DiscordManager();

    private DiscordManager() { }

    public static DiscordManager getInstance() {
        return instance;
    }

    public void init() {
        discordApi.connect(new FutureCallback<DiscordAPI>() {

            @Override
            public void onSuccess(DiscordAPI api) {
                api.registerListener(new MessageCreateListener() {
                    @Override
                    public void onMessageCreate(DiscordAPI api1, Message message) {
                        InputManager.getInstance().scanForTriggers(message);
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }

        });
        setPlaying(PLAYING);
    }

    public void setPlaying(String text) {
        discordApi.setGame("\uD83D\uDD34 " + text);
    }

}
