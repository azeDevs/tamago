package main.java.bot.managers;

import de.btobastian.javacord.entities.message.Message;
import main.java.bot.Bot;

public class OutputManager {

    private static final OutputManager instance = new OutputManager();

    private OutputManager() { }

    public static OutputManager getInstance() {
        return instance;
    }

    public String getKeyTriggeredResponse(Message message) {
        String outgoing = message.getContent().substring(5);
        if (message.getContent().length() == 4) message.reply("`ᴛᴀᴍᴀɢᴏ … " + message.getAuthor().getName() + "`");
        if (outgoing.contains("status")) getStatus();
        return "";
    }

    public String getFreeTriggeredResponse(Message message) {
        if (message.getContent().contains("pingo")) return "```\n" +
                "· · · · · · · · · · · · · · · · · · · · · · · · · · · \n" +
                "· · · · · · · · · · · · · · · ████· · · · · · · · · · \n" +
                "· · · · · · · · · · · · · · ██· · ██· · · · pongo · · \n" +
                "· · · · · · · · ████████████· · · · ██· · · · · · · · \n" +
                "· · · · · · ████· · · · · · · · · · · ██· · · · · · · \n" +
                "· · · · · ██· · · · · · · · · · · · · · ██· · · · · · \n" +
                "· · · · ██· · · · · · · · · · · · · · · · ██· · · · · \n" +
                "· · · · ██· · · · · · · · · · · · · · · · ██· · · · · \n" +
                "· · · ██· · · ██· · · · · · · · ██· · · · · ██· · · · \n" +
                "· · · ██· · · · · · · ████· · · · · · · · · ██· · · · \n" +
                "· · · ██· · · · · · ██· · ██· · · · · · · · ██· · · · \n" +
                "· · · ██· · · · · · ██· · ██· · · · · · · · · ██· · · \n" +
                "· · · ██· · · · · · · ████· · · · · · · · · · ██· · · \n" +
                "· · ██· · · · · · · · · · · · · · · · · · · · · ██· · \n" +
                "· · ██· · · · · · · · · · · · · · · · · · · · · ██· · \n" +
                "· · · ██· · · · · · · · · · · · · · · · · · ████· · · \n" +
                "· · · · ████████· · · · · · · · · · · ██████· · · · · \n" +
                "· · · · · · · · ██████████████████████· · · · · · · · \n" +
                "· · · · · · · · · · · · · · · · · · · · · · · · · · · \n" + "```";
        if (message.getContent().contains(Bot.ALIAS)) return "```\n" +
                "· · · · · · · · · · · · · · · · · · · · · · · · · · · \n" +
                "· · · · · · · · · · · · · · · ████· · · · · · · · · · \n" +
                "· · · · · · · · · · · · · · ██· · ██· · · · · · · · · \n" +
                "· · · · · · · · ████████████· · · · ██· · · · · · · · \n" +
                "· · · · · · ████· · · · · · · · · · · ██· · · · · · · \n" +
                "· · · · · ██· · · · · · · · · · · · · · ██· · · · · · \n" +
                "· · · · ██· · · · · · · · · · · · · · · · ██· · · · · \n" +
                "· · · · ██· · · · · · · · · · · · · · · · ██· · · · · \n" +
                "· · · ██· · · ██· · · · · · · · ██· · · · · ██· · · · \n" +
                "· · · ██· · · · · · · ████· · · · · · · · · ██· · · · \n" +
                "· · · ██· · · · · · ██· · ██· · · · · · · · ██· · · · \n" +
                "· · · ██· · · · · · ██· · ██· · · · · · · · · ██· · · \n" +
                "· · · ██· · · · · · · ████· · · · · · · · · · ██· · · \n" +
                "· · ██· · · · · · · · · · · · · · · · · · · · · ██· · \n" +
                "· · ██· · · · · · · · · · · · · · · · · · · · · ██· · \n" +
                "· · · ██· · · · · · · · · · · · · · · · · · ████· · · \n" +
                "· · · · ████████· · · · · · · · · · · ██████· · · · · \n" +
                "· · · · · · · · ██████████████████████· · · · · · · · \n" +
                "· · · · · · · · · · · · · · · · · · · · · · · · · · · \n" + "```";
        return "";
    }


    public String getStatus() {
        return "```md\n" +
                "Tamago\n" +
                "[ HITPOINTS ][ " + getBarString(4, 8) + " ]\n" +
                "[ NUTRIENTS ][ " + getBarString(4, 8) + " ]\n\n" +
                "Lifespan: ?" +
                "```";
    }

    private String getBarString(int fill, int max) {
        String outgoing = "";
        outgoing = "▰▰▰▰▰▱▱▱";
//        for (int n = hp; n > 0; n--) {
//            System.out.println("HP="+hp + " N="+n + " BAR="+outgoing);
//            outgoing += "▰";
//        }
//        for (int n = hp; n < 8; n++) {
//            System.out.println("HP="+hp + " N="+n + " BAR="+outgoing);
//            outgoing += "▱";
//        }
//        System.out.println("HP="+hp + " FINAL BAR="+outgoing);
        return outgoing;
    }

}
